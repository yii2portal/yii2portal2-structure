<?php

namespace yii2portal\structure\common\models;
use creocoder\nestedsets\NestedSetsQueryBehavior;

/**
 * This is the ActiveQuery class for [[Structure]].
 *
 * @see Structure
 */
class StructureQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     * @return Structure[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Structure|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
