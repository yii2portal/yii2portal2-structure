<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model yii2portal\structure\common\models\Structure */

$this->title = Yii::t('yii2portal/access', 'Update {modelClass}: ', [
    'modelClass' => 'Structure',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii2portal/access', 'Structures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('yii2portal/access', 'Update');
?>
<div class="structure-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
