<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model yii2portal\structure\common\models\Structure */

$this->title = Yii::t('yii2portal/access', 'Create Structure');
$this->params['breadcrumbs'][] = ['label' => Yii::t('yii2portal/access', 'Structures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="structure-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
