<?php

use yii\db\Migration;

class m170415_183807_init extends Migration
{
    public function up()
    {
        $this->createTable('{{%structure}}', [
            'id' => $this->primaryKey(),
            'tree' => $this->integer()->notNull(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'module' => $this->string()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%structure}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
