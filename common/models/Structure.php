<?php

namespace yii2portal\structure\common\models;

use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "{{%structure}}".
 *
 * @property integer $id
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $title
 * @property string $slug
 * @property string $module
 */
class Structure extends \yii2portal\core\backend\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%structure}}';
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     * @return StructureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StructureQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'slug'=>[
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
            ],
            'tree' => [
                'class' => NestedSetsBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'module'], 'required'],
            [['title',], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii2portal/structure', 'ID'),
            'tree' => Yii::t('yii2portal/structure', 'Tree'),
            'lft' => Yii::t('yii2portal/structure', 'Lft'),
            'rgt' => Yii::t('yii2portal/structure', 'Rgt'),
            'depth' => Yii::t('yii2portal/structure', 'Depth'),
            'title' => Yii::t('yii2portal/structure', 'Title'),
            'slug' => Yii::t('yii2portal/structure', 'Slug'),
            'module' => Yii::t('yii2portal/structure', 'Module'),
        ];
    }
}
